FROM jupyter/minimal-notebook@sha256:b3dd6b2f13c99c57d876a8d03424f36c052906e736c340e8f448fbeae6d5e8e3

USER root
RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
    libgl1-mesa-dev \
    ffmpeg

USER ${NB_UID}

COPY --chown=${NB_UID}:${NB_GID} . ${HOME}

RUN pip install --no-cache-dir notebook
RUN pip install --no-cache-dir ipywidgets matplotlib numpy opencv-python



