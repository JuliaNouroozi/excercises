import math
import time

import cv2 as cv
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.backend_bases import MouseButton

class AxesData():

    def __init__(self, axes, img):
        self.axes_image = axes.imshow(img)
        self.img = img
        self.rect_start = None

class PixelSelectionTool():

    def __init__(self, images, movement_filter_ms=100, rect_color=(0, 255, 0), rect_thickness=1, figsize=None, visualize_rect=True):
        self.imgs = list(map(lambda img_name: cv.imread(img_name, cv.IMREAD_COLOR), images))
        self.imgs = list(map(lambda img: cv.cvtColor(img, cv.COLOR_BGR2RGB), self.imgs))

        cols = 3
        rows = math.ceil(len(self.imgs) / cols)
        
        if not figsize:
            self.fig, self.axs = plt.subplots(rows, cols)
        else:
            self.fig, self.axs = plt.subplots(rows, cols, figsize=figsize)

        self.map = {}
        for i in range(rows):
            for j in range(cols):
                idx = i * cols + j
                if idx >= len(self.imgs):
                    continue

                ax = self.axs[i, j]
                ax.axis('off')
                img = self.imgs[i * cols + j]
                self.map[ax] = AxesData(ax, img)

        self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        self.fig.canvas.mpl_connect('motion_notify_event', self.onmove)

        self.last_move_event = time.time()
        self.movement_filter_ms = movement_filter_ms

        self.rect_color = rect_color
        self.rect_thickness = rect_thickness

        self.observer = None
        self.visualize_rect = visualize_rect

    def show(self):
        plt.show()

    def onclick(self, event):
        # skip if event is not on an image
        if not event.inaxes:
            return

        if event.inaxes not in self.map:
            return

        x, y = int(event.xdata), int(event.ydata)
        axes_data = self.map[event.inaxes]

        if event.button == MouseButton.LEFT:
            rect_start = axes_data.rect_start
            # print('Left mouse pressed!')
            if axes_data.rect_start:
                skin_pixels = []
                for i in range(min(rect_start[0], x), max(rect_start[0], x)):
                    for j in range(min(rect_start[1], y), max(rect_start[1], y)):
                        skin_pixels.append(axes_data.img[i, j, ::-1])

                cv.rectangle(axes_data.img, rect_start, (x, y), (0, 255, 0), 1)
                axes_data.rect_start = None
                if self.observer:
                    self.observer(skin_pixels)
            else:
                axes_data.rect_start = (x, y)
        elif event.button == MouseButton.RIGHT:
            axes_data.rect_start = None

        self.redraw(axes_data) 


    def onmove(self, event):
        if not self.visualize_rect:
            return

        if not event.inaxes:
            return

        if event.inaxes not in self.map:
            return

        _time_passed_ms = (time.time() - self.last_move_event) * 1000
        if _time_passed_ms < self.movement_filter_ms:
            return
        self.last_move_event = time.time()

        axes_data = self.map[event.inaxes]
        if not axes_data.rect_start:
            return

        x, y = int(event.xdata), int(event.ydata)
        img = axes_data.img.copy()
        cv.rectangle(img, axes_data.rect_start, (x, y), self.rect_color, self.rect_thickness)
        self.redraw(axes_data, img=img)

    def redraw(self, axes_data, img=None):
        _img = axes_data.img if img is None else img
        axes_data.axes_image.set_data(_img)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def on_new_pixels(self, observer):
        self.observer = observer


def change(q, item):
    try:
        q.put_nowait(item)
    except queue.Full:
        q.get_nowait()
        q.put_nowait(item)

if __name__ == '__main__':
    import os

    pixels = [] 
    img_dir = './resources/skin_images/'
    images = [os.path.join(img_dir, f) for f in os.listdir(img_dir)]
    tool = PixelSelectionTool(images)

    tool.on_new_pixels(lambda new_pixels: pixels.extend(new_pixels))
    tool.show()
    print(f'Pixels: {len(pixels)}')
